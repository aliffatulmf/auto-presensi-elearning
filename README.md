

# Auto Presensi E-Learning
Program ini dibuat karena keresahan saya terhadap adanya jam kuliah pagi, dikarenakan setiap hari harus melakukan presensi secara online. Jika saya bangun kesiangan dapat saya antisipasi dengan cara membuat bot ini berjalan setiap jamnya atau pada kurun waktu tertentu. 

## Requirements
- Linux amd64 (Ubuntu 20.04 recommended)
- Python 3.8
- Firefox latest version
- Geckodriver (*GitHub https://github.com/mozilla/geckodriver/releases*)

## Install
!!! UNSTABLE !!!
```
$ git clone https://gitlab.com/aal18/auto-presensi-elearning.git
$ cd auto-presensi-elearning
$ pip install -r requirements.txt --no-cache-dir
$ sudo python3 install.py
```

## Arguments
Format info.cfg
```shell
['user']
nim=123.456.789             # nim
password=s3cr3t             # password
driver=./driver/geckodriver # driver location (default)
```

`nim` nomor atau identitas elearning<br>
`password` kata sandi elearning<br>
`driver` letak file webdriver *(default: ./driver/geckodriver)*