import os
import shutil
import subprocess
import sys

if __name__ == "__main__":
    dest = '/opt/ape'
    cron = '/etc/crontab'
    cronBackup = '/etc/crontab.backup'
    opt_location = os.path.isdir(dest)

    if not opt_location:
        try:
            os.mkdir(dest)
            os.mkdir(f'{dest}/driver')
            shutil.copyfile('automa.py', '/opt/ape/automa.py')
            shutil.copyfile('configuration.py', '/opt/ape/configuration.py')
            shutil.copyfile('info.cfg.sample', '/opt/ape/info.cfg')
            shutil.copyfile('jadwal.json', '/opt/ape/jadwal.json')
            shutil.copyfile('info.cfg.sample', '/opt/ape/info.cfg')
            shutil.copyfile('driver/DRIVER_LOCATION', '/opt/ape/driver/DRIVER_LOCATION')

        except IOError as e:
            sys.exit('Please run as root!')

    # if not os.path.isfile(cronBackup):
    #     try:
    #         shutil.copyfile(cron, cronBackup)
    #     except IOError as e:
    #         sys.exit('Please run as root!')
