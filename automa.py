import datetime

from selenium.webdriver import Firefox
from selenium.webdriver.common.keys import Keys

from configuration import (JadwalConfig, Message, PathConfig, delay, UserModel)

driver = Firefox(executable_path=PathConfig.DRIVER)


def login():
    driver.get('https://elearning.unisbank.ac.id/login/')
    driver.find_element_by_id('username').send_keys(UserModel['user']['nim'])
    driver.find_element_by_id('password').send_keys(
        UserModel['user']['password'], Keys.ENTER)

    delay(5, 9).dynamic()
    try:
        if driver.find_element_by_partial_link_text(
                Message.INVALID_LOGIN).size() != 0:
            raise Exception(Message.INVALID_LOGIN)
    except Exception:
        return 1
    return 0


def find_makul(makul):
    driver.find_element_by_partial_link_text(str(makul)).click()
    delay(5, 9).dynamic()
    driver.get('https://elearning.unisbank.ac.id/my/')
    return 1


def logout():
    driver.find_element_by_css_selector('#action-menu-toggle-1').click()
    driver.find_element_by_partial_link_text('Keluar').click()
    return 1


if __name__ == "__main__":
    if login():
        jadwal = JadwalConfig()
        today = datetime.datetime.now().strftime('%A').lower()
        try:
            for makul in jadwal.scan(today):
                delay(3, 5).dynamic()
                find_makul(makul)
            delay(3).static()
            if logout():
                driver.quit()
        except Exception:
            pass
    else:
        driver.quit()
